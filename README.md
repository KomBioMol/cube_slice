# Cube slice

Cube slice is a Python library for manipulating with cube files and making pretty visualisations.

## Installation 

For now you need to clone repo and install using pip.
```bash
git clone https://gitlab.com/KomBioMol/cube_slice.git
pip3 install ./cube_slice
```

## Usage

See examples.

## Contribution

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

GNU GPLv3
