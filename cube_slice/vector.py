import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
# WORKING VERSION




class Vector:

    def __init__(self, ax, quiv, origin, coords):
        self.ax = ax
        self.quiv = quiv
        self.origin = origin
        self.coords = coords
        self.press = None

    def connect(self):
        """connect to all the events we need"""
        self.cidpress = self.quiv.figure.canvas.mpl_connect(
            'button_press_event', self.on_press)
        self.cidrelease = self.quiv.figure.canvas.mpl_connect(
            'button_release_event', self.on_release)
        self.cidmotion = self.quiv.figure.canvas.mpl_connect(
            'motion_notify_event', self.on_motion)

    def on_press(self, event):
        """on button press we will see if the mouse is over us and store some data"""
        if event.inaxes != self.quiv.axes:
            return

        contains, attrd = self.quiv.contains(event)
        if not contains:
            return
        self.press = self.ax.azim, self.ax.elev

    def on_motion(self, event):
        """on motion we will move the rect if the mouse is over us"""
        if self.press is None:
            return
        if event.inaxes != self.quiv.axes:
            return

        az = self.ax.azim - self.press[0]
        el = self.ax.elev - self.press[1]

        rot = Vector.rot_matrix([0,0,1], az, degrees=True)
        self.coords = rot.dot(self.coords)
        self.origin = rot.dot(self.origin)
        axis = np.cross([0,0,1], [np.cos(self.ax.azim * np.pi / 180.), np.sin(self.ax.azim * np.pi / 180.), 0])
        rot = Vector.rot_matrix(axis, -el, degrees=True)
        self.coords = rot.dot(self.coords)
        self.origin = rot.dot(self.origin)

        self.quiv.set_segments([[self.origin, self.coords]])
        self.quiv.figure.canvas.draw()

        self.press = self.ax.azim, self.ax.elev

    def on_release(self, event):
        """on release we reset the press data"""
        self.press = None
        self.quiv.figure.canvas.draw()

    def disconnect(self):
        """disconnect all the stored connection ids"""
        self.quiv.figure.canvas.mpl_disconnect(self.cidpress)
        self.quiv.figure.canvas.mpl_disconnect(self.cidrelease)
        self.quiv.figure.canvas.mpl_disconnect(self.cidmotion)

    @staticmethod
    def rot_matrix(axis, angle, degrees=False):
        assert np.array(axis).shape == (3,)
        assert isinstance(angle, float)

        if degrees:
            a = angle * np.pi / 180.
        else:
            a = angle

        if np.linalg.norm(axis) != 1:
            axis /= np.linalg.norm(axis)

        ux, uy, uz = axis
        rot = np.array(
            [[np.cos(a)+(1-np.cos(a))*ux**2,    ux*uy*(1-np.cos(a))-uz*np.sin(a), ux*uz*(1-np.cos(a))+uy*np.sin(a)],
             [ux*uy*(1-np.cos(a))+uz*np.sin(a), np.cos(a)+(1-np.cos(a))*uy**2,    uy*uz*(1-np.cos(a))-ux*np.sin(a)],
             [uz*ux*(1-np.cos(a))-uy*np.sin(a), uz*uy*(1-np.cos(a))+ux*np.sin(a), np.cos(a)+(1-np.cos(a))*uz**2]])
        return rot

    @staticmethod
    def rotate_vector_around(vec, axis, angle, deg=True):
        """TODO: Docstring for rotate_vector_around.

        :vec: TODO
        :axis: TODO
        :angle: TODO
        :returns: TODO

        """
        axis = axis / np.linalg.norm(axis)
        if deg:
            angle = np.pi*angle/180 
        vecrot = vec*np.cos(angle) + np.cross(axis, vec)*np.sin(angle) + axis*np.dot(axis, vec)*(1-np.cos(angle))
        return vecrot


def main():
    origin = np.array([0, 1, 0])
    normal = np.array([1, 2, 1])

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d', proj_type='ortho')
    ax.scatter(np.random.rand(10), np.random.rand(10), np.random.rand(10))
    quiw = ax.quiver(*origin, *normal, color='red')
    ax.set_xlim([-2, 2])
    ax.set_ylim([-2, 2])
    ax.set_zlim([-2, 2])

    azim0 = ax.azim
    elev0 = ax.elev

    s = Vector(ax, quiw, origin, normal)
    s.connect()

    plt.show()

if __name__ == '__main__':
    main()
