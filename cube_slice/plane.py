import numpy as np
from .cube import Cube

class Plane:

    def __init__(self):
        self.normal = np.array([1,0,0])
        self.origin = np.array([0,0,0])
        self.delta = 1.0
        self.cube = None

    def set_cube(self, cube):
        assert isinstance(cube, Cube)
        self.cube = cube

    def set_normal(self, vec):
        try:
            vec = np.array(vec)
        except Exception as ex:
            print(ex)
        assert vec.shape == (3,)
        self.normal = vec/np.linalg.norm(vec)

    def set_origin(self, vec):
        try:
            vec = np.array(vec)
        except Exception as ex:
            print(ex)
        assert vec.shape == (3,)
        self.origin = vec

    def equation(self, xyz):
        xyz = np.array(xyz)
        return self.normal.dot(xyz - self.origin)

    def distance(self, x, y, z, abs=True):
        D = - np.sum(self.normal*self.origin)
        denominator = np.sqrt(np.sum(self.normal**2))
        numerator = self.normal[0]*x+self.normal[1]*y+self.normal[2]*z+D
        if abs:
            return np.abs(numerator/denominator)
        else:
            return numerator / denominator

    def choose_points(self):
        return np.where(self.distance(self.cube.x, self.cube.y, self.cube.z)<self.delta)

    def points_projection(self):
        self.x = self.cube.x[self.choose_points()] \
                 - self.normal[0]*self.distance(self.cube.x[self.choose_points()], self.cube.y[self.choose_points()],
                                                self.cube.z[self.choose_points()], abs=False)
        self.y = self.cube.y[self.choose_points()] \
                 - self.normal[1]*self.distance(self.cube.x[self.choose_points()], self.cube.y[self.choose_points()],
                                                self.cube.z[self.choose_points()], abs=False)
        self.z = self.cube.z[self.choose_points()] \
                 - self.normal[2]*self.distance(self.cube.x[self.choose_points()], self.cube.y[self.choose_points()],
                                                self.cube.z[self.choose_points()], abs=False)

    def get_values(self):
        self.values = self.cube.values[self.choose_points()]

    def generate_new_basis(self):
        # implemented: https://stackoverflow.com/questions/8780646/mapping-coordinates-from-plane-given-by-normal-vector-to-xy-plane?rq=1
        assert self.normal[2] != 0.0
        self.bvX = np.array([1, 0, -self.normal[0]/self.normal[2]])
        # self.bvY = np.array([0, 1, -self.normal[1]/self.normal[2]]) # this thing is not creating orthogonal base
        self.bvY = np.cross(self.normal, self.bvX)

        self.bvX = 10*self.bvX / np.linalg.norm(self.bvX)
        self.bvY = 10*self.bvY / np.linalg.norm(self.bvY)
        return self.bvX, self.bvY

    def transform_points(self):
        # TODO transform points to new basis
        # print(self.x, self.y, self.z)
        self.Y = self.y / self.bvY[1]
        self.X = ( self.x - self.Y * self.bvY[0] ) / self.bvX[0]
        # print(self.X, self.Y)
        fig = plt.figure()
        plt.scatter(self.X, self.Y, c=self.values)

    def interpolate(self):
        new_X = np.arange(self.X.min()-1, self.X.max()+1, 0.01)
        new_Y = np.arange(self.Y.min()-1, self.Y.max()+1, 0.01)

        f = interp2d(self.X, self.Y, self.values, kind='linear', fill_value=10, bounds_error=False)

        new_values = f(new_X, new_Y)

        # fig = plt.figure()
        plt.pcolormesh(new_X, new_Y, new_values, zorder=0)

    def slice_volume(self, sign='+'):
        assert sign in ['+', '-']
        # print(self.equation(list(self.origin)))

        for i in range(len(self.cube.values)):
            # print(self.equation([self.cube.x[i], self.cube.y[i], self.cube.z[i]]))
            if sign == '+' and self.equation([self.cube.x[i], self.cube.y[i], self.cube.z[i]]) < 0:
                self.cube.values[i] = 0
            elif sign == '-' and self.equation([self.cube.x[i], self.cube.y[i], self.cube.z[i]]) > 0:
                self.cube.values[i] = 0
