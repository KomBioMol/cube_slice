import matplotlib
matplotlib.use('TkAgg')
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3


class Molecule:
    """
    Object that holds molecule from cube/pdb file
    
    Attributes
    ----------
    natoms : int
        number of atoms
    nframes : int
        number of frames
    xyz : ndarray
        coordinates of atoms, shape (nframes, natoms, 3)
    names : ndarray of floats
        symbols of atoms, shape (natoms)
    masses : ndarray of floats
        masses of atoms
    numbers : ndarray of ints
        atomic numbers
    colors : ndarray of strings
        colors of atoms (for visualisation purposes)
    pairs : list of tuples
        pairs of atoms which form bonds (for visualisation purposes)
    """
    def __init__(self):
        self.natoms = None
        self.nframes = None
        self.xyz = []
        self.names = []
        self.masses = []
        self.numbers = []
        self.colors = []
        self.pairs = []

    def read_pdb(self, filename): #FIXME replace this ad hoc solution to something more general
        self.xyz = np.array([[float(x) for x in line.split()[5:8]] for line in open(filename)
                        if line.startswith("ATOM")]).reshape(self.nframes, self.natoms, 3)
        self.names = np.array([line.split()[2][0] for line in open(filename) if line.startswith("ATOM")])[:self.natoms]

    def atom2color(self): #FIXME move those dicts to separate class
        atoms_to_colors = {"C": 'k', "O": 'r', "N": 'b', "H": 'w', "P": 'y'}
        self.colors = np.array([atoms_to_colors[x] for x in self.names])

    def number2atom(self):
        number_to_atoms = {6: "C", 8: "O", 7: "N", 1: "H", 15:"P"}
        self.names = np.array([number_to_atoms[x] for x in self.numbers])

    def atom2number(self):
        atoms_to_numbers = {'H':1, 'C':6, 'N':7, 'O':8, 'P':15}
        self.numbers = np.array([atoms_to_numbers[x] for x in self.names])

    def atom2masses(self):
        atoms_to_masses = {'H':1.01, 'C':12.01, 'N':14.01, 'O':16.00, 'P':30.97}
        self.masses = np.array([atoms_to_masses[x] for x in self.names])
        

    def ignh(self):
        condition = np.where(self.numbers != 1)
        self.numbers = self.numbers[condition]
        self.names = self.names[condition]
        self.colors = self.colors[condition]
        self.xyz = self.xyz[:,condition[0],:]

    def find_pairs(self, lower=0.1, upper=3.0):
        xyz_s = self.xyz[0,:,:]
        for i, first_atom in enumerate(xyz_s):
            for j, second_atom in enumerate(xyz_s):
                if lower < np.linalg.norm(first_atom-second_atom) < upper:
                    self.pairs.append((i, j))

    def plot(self):
        fig = plt.figure(figsize=(6, 6))
        ax = plt.subplot(111, projection='3d')
        ax.set_aspect('equal')
        xyz_s = self.xyz[0]
        box_size = max(np.max(xyz_s, 0) - np.min(xyz_s, 0)) + 1
        mins = np.mean(xyz_s, 0)
        ax.set_xlim(mins[0] - box_size / 2, mins[0] + box_size / 2)
        ax.set_ylim(mins[1] - box_size / 2, mins[1] + box_size / 2)
        ax.set_zlim(mins[2] - box_size / 2, mins[2] + box_size / 2)
        s = np.array(self.numbers)*3
        points = ax.scatter(xyz_s[:, 0], xyz_s[:, 1], xyz_s[:, 2], c=self.colors, s=s, edgecolors='k', depthshade=True)
        lines = [ax.plot([xyz_s[i,0], xyz_s[j,0]], [xyz_s[i,1], xyz_s[j,1]], [xyz_s[i,2], xyz_s[j,2]], c='k', linewidth=1)[0] for i,j in self.pairs]
        return fig, ax

    def compute_com(self):
        com = np.average(self.xyz, weights=self.masses, axis=1)
        return com

    def compute_inertia_tensor(self):
        xyz = self.xyz - self.compute_com()[:,np.newaxis,:]
        E = np.swapaxes(np.tile(np.identity(3), self.natoms), 0, 1).reshape(self.natoms,3,3)
        E = np.moveaxis(np.tile(E,self.nframes).reshape(self.natoms,3,self.nframes,3), 2, 0)

        r = np.einsum('ijk,ijk->ij', xyz, xyz)
        rr = np.einsum('ijk,ijl->ijkl', xyz, xyz)

        I = E*r[:,:,np.newaxis,np.newaxis] - rr
        I = self.masses[np.newaxis,:,np.newaxis,np.newaxis]*I
        I = I.sum(axis=1)
        return I    

    def compute_principal_axis(self, axis=0):
        com = self.compute_com()
        I = self.compute_inertia_tensor()
        eigvals, eigvecs = np.linalg.eig(I)
        return eigvecs[:,:,axis]


def main():
    m = Molecule()
    m.natoms = 63
    m.nframes = 1
    m.read_pdb("test/conf.pdb")
    m.atom2color()
    m.atom2number()
    m.atom2masses()
    print(m.xyz.shape)
    print(m.masses.shape)
    print(m.compute_com())
#     m.ignh()
    
    axis0 = m.compute_principal_axis(0)
    axis1 = m.compute_principal_axis(1)
    axis2 = m.compute_principal_axis(2)

    m.find_pairs(0.1, 1.62)
    fig, ax = m.plot()
    ax.scatter(*m.compute_com()[0])
    ax.quiver(*m.compute_com()[0], *axis0[0], color='red')
    ax.quiver(*m.compute_com()[0], *axis1[0], color='green')
    ax.quiver(*m.compute_com()[0], *axis2[0], color='blue')
    plt.show()

if __name__ == '__main__':
    main()
