from setuptools import setup

setup(name='cube_slice',
      version='0.1',
      description='Library to manipulate with cube files',
      url='https://gitlab.com/KomBioMol/cube_slice',
      author='Cyprian Kleist',
      author_email='Cyp2505@wp.pl',
      license='GNU GPLv3',
      packages=['cube_slice'],
      zip_safe=False)
