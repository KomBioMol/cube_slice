from cube_slice import *

c = Cube('whole.cube')


p = Plane()
p.set_cube(c)

c.molecule.atom2masses()
p.set_origin(c.molecule.compute_com()[0])

princ0 = c.molecule.compute_principal_axis(0)[0]
princ1 = c.molecule.compute_principal_axis(1)[0]
princ2 = c.molecule.compute_principal_axis(2)[0]
axis = Vector.rotate_vector_around(princ0, princ2, 5)

p.set_normal(axis)
p.slice_volume()

p.cube.write('sliced.cube')
