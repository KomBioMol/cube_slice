from cube_slice import *


c = Cube('ugly.cube')
c.interpolate()

c.create_new_grid(20,20,20)

c.soften_data(2)
c.write('pretty.cube')
